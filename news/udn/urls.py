from django.conf.urls import url
from udn import views


urlpatterns = [
    url(r'^posts$', views.post_list),
    url(r'^posts/(?P<pk>[0-9]+)/$', views.post_detail),
]
