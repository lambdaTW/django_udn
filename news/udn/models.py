from django.db import models

# Create your models here.
class Post(models.Model):
    url = models.TextField(unique=True)
    title = models.CharField(max_length=100, blank=False)
    content = models.TextField()
    time = models.DateTimeField()
    crawler_time = models.DateTimeField(auto_now_add=True)
    image = models.ImageField()

    class Mata:
        ordering = ('time')
        unique_together = ('url', 'time')
