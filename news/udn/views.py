from datetime import datetime
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from udn.models import Post
from udn.serializers import PostSerializer, PostsSerializer

# Create your views here.
@csrf_exempt
def post_list(request):
    if request.method == 'GET':
        time = request.GET.get('t', '1900-01-01 00:00')
        time = datetime.strptime(time, '%Y-%m-%d %H:%M')
        posts = Post.objects.filter(time__range=[time, datetime.now()])
        serializer = PostsSerializer(posts, many=True)
        return JsonResponse(serializer.data, safe=False)

    return JsonResponse(serializer.errors, status=400)

@csrf_exempt
def post_detail(request, pk):
    try:
        post = Post.objects.get(pk=pk)
    except Post.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = PostSerializer(post)
        return JsonResponse(serializer.data)

    return HttpResponse(status=404)
