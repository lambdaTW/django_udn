from udn.models import Post
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from udn.serializers import PostSerializer
from datetime import datetime

d = datetime.strptime('2017-12-11 12:50', '%Y-%m-%d %H:%M')
post = Post(title='hello', content='world', time=d, image='123456')
post.save()
serializer = PostSerializer(post)

serializer.data
