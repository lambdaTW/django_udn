from nba import MySpider
from scrapy.crawler import CrawlerProcess


def main():
    PROCESS = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })

    PROCESS.crawl(MySpider)
    PROCESS.start()

if __name__ == '__main__':
    main()
