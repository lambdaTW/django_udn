import os
import django
from datetime import datetime
from lxml import html
import requests
import scrapy


# Django setup, this must done before you import your models
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'news.settings')
django.setup()

from udn.models import Post

class MySpider(scrapy.Spider):
    name = 'nba.udn'
    base_url = 'https://nba.udn.com'


    def start_requests(self):
        ug = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'
        conn = requests.Session()
        conn.headers.update({'User-Agent': ug})
        r = conn.get('https://nba.udn.com/nba/index?gr=www')
        paser = html.fromstring(r.content)
        news = paser.xpath('//*[@id="news_body"]/dl/dt/a/@href')
        for new in news:
            url = self.base_url + new
            yield scrapy.Request(url=url, callback=self.page_parse)

    def page_parse(self, response):
        img_name = None
        url =response.url
        title = response.xpath('//h1[@class="story_art_title"]/text()').extract_first()
        content = ''.join([p for p in response.xpath('//div[@id="story_body_content"]/span//text()').extract()])
        time = response.xpath('//div[@class="shareBar__info--author"]/span/text()').extract_first()
        time = datetime.strptime(time, '%Y-%m-%d %H:%M')
        img_url = response.xpath('//div[@id="story_body_content"]/span/figure/a/img/@data-src').extract_first()

        if img_url:
            img_name = img_url.split('/')[-1].split('.')[0]
            with open('/var/www/news/media/{name}.jpg'.format(name=img_name), 'wb') as img_f:
                client = requests.Session()
                image = client.get(url=img_url, stream=True)
                for chunk in image:
                    img_f.write(chunk)

        try:
            post = Post(url=url, title=title, content=content, time=time, image=img_name)
            post.save()
        except Exception:
            pass
